@Value
related to application.properties file 
application.yaml

# sb_108
ThymeLeaf tags, Model <-- List of stories <- Story POJO

## 1st to build, then run

*	model.addAttribute("pageTitle", "SFJ mindig");
*	<title th:text="${pageTitle}">Napi SFJ</title>

## 2nd to build, then run

### .contoller / @Controller class

* @GetMapping function return "stories"; -> stories.html
* Model model.addAttributes("stories", getStories()); 
* private ArrayList<Story> getStories() { ... } gives back a List with 2 Story POJO objects with datas


### .domain / Story POJO
with empty and full constructors, getters, setters, @Override toString()
*   private String title;
*   private Date posted;
*	private String content;
*	private String author;

### resources/templates/ stories.html

starting-point is always resources/static:
    
    <link rel="stylesheet" th:href="@{css/mymain.css}" href="../static/css/main.css"/>

Leave it as download with cdn is fast:

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    
    <article th:each="story:${stories}">
    <h1 th:text="${story.title}">
    <section th:utext="${story.content}"> .... for "<p>anything text</p>"

### Free Super  Bootstrap builder 2019
* [Bootstrap website builder 2019](https://mobirise.com/_l/bootstrap-website-builder/?gclid=EAIaIQobChMI-a-TvurI6AIVRvZ3Ch0atwBKEAEYASAAEgJwlfD_BwE)   

## ThymeLeaf tags

    th:if="${#lists.isEmpty(files)}"
    th:unless="${#lists.isEmpty(files)}"

    <html .... xmlns:th="http://www.thymleaf.org"> 
    
    <title th:text="${pageTitle}">Napi SFJ</title>

**cdn link for bootstrap access is pretty speedy:**    
    
    <link rel="stylesheet" th:href="@{css/mymain.css}" href="../static/css/main.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

**link for Google Fonts**

    <link href='https://fonts.googleapis.com/css?family=Calibri' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Exo' rel='stylesheet' type='text/css' />  

**stories is a Model map key, with value of the List of Story-POJO entities**

    <article th:each="story:${stories}">
        <header>
            <h1 th:text="${story.title}">Ez itt a cím</h1> 
            <p  th:text="${#dates.format(story.posted,'yyyy.MM.dd HH:mm:ss')}">Posted on September 31, 2015 at 10:00 PM</p>
        </header>
    
 	    <section th:utext="${story.content}">
 	    <p>Carl told that....</p>
        </section>
        
        <footer>
            <address th:text="${story.author}">Beküldte: <span>Anonymous</span>             
            </address>
        </footer>
        <hr/>
    </article>

## .css

* **css for a class=**

    .navbar {
       margin-top:15px;
    }

* **css for the header/h1 html tag**
    
     header h1 {
       font-family: 'Exo', sans-serif;
       font-size: 36px;
       background-color: coral;
     }

* **css for <p> OR <address> html tags**

    p, address {
      font-size: large;
    }

* **for <h1>**

    h1 {
      font-family: 'Exo', sans-serif;
      font-size: 72px;
      background-color: coral;
    }


package siample.dev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@EnableConfigurationProperties
@SpringBootApplication
public class Sb108Application {

	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb108Application.class, args);
		System.out.println(aContext.getBean("person"));
	}

	
}
